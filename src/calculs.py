# -*- coding: UTF-8 -*-
'''
Created on 2014-02-18
Updated on 2014-10-25 for Python 3.4
@author: Johnny Tsheke - UQAM
'''

from donnees import matrices

tableau=matrices.tab #on prend la variable importee

somme=0

for nb in tableau: #pour chaque element de la liste (tableau)
    somme=somme+nb # ici nb est la variable d'itération

print("Somme caclulée avec 'for':",somme)

somme2=0
nombreElements=len(tableau) #nombre d'élément dans la liste
indice=0


while indice < nombreElements : # ici indice est la variable d'iteration
    somme2=somme2+tableau[indice] #indice est l'indice du tableau. pas l'élément lui-même
    indice=indice+1

print("Somme caclulée avec 'while':",somme2)