# -*- coding: UTF-8 -*-
'''
Created on 2014-02-18
Updated on 2014-10-25 for Python 3.4
@author: Johnny Tsheke - UQAM
'''
#cette manière d'écrire facilite la lisibilité

employes=[{"Prenom":"Martin","Salaire":1230.50},
           {"Prenom":"Tram","Salaire":1837.00},
           {"Prenom":"Kadima","Salaire":2230.50},
           {"Prenom":"Mamadou","Salaire":870.95},
           {"Prenom":"Jacques","Salaire":2230.50}
          ]