# -*- coding: UTF-8 -*-
'''
Created on 2014-02-18
Updated on 2014-10-25 for Python 3.4
@author: Johnny Tsheke - UQAM
'''

from donnees import personnes

liste=personnes.employes #ici 'liste' est une variable. je peux l'appeler comme je veux

#Il y a plusieurs stratégie pour résoudre ce problème.
#choisit la stratégie consistant à trouver d'abord les salaires min et max
#Ici on presume que le salaire est un nombre positif

salaireMin=0 #Salaire minimal
salaireMax=0 #Salaire maximal
sommeSalaires=0
nombreEmployes=len(liste)
for emp in liste : #pour chaque employé, on compare son salaire avec les min et max
    if emp["Salaire"] > salaireMax:
        salaireMax=emp["Salaire"] 
    
    if (emp["Salaire"] < salaireMin) or (salaireMin==0):
        salaireMin=emp["Salaire"]
        
    sommeSalaires=sommeSalaires+emp["Salaire"]
#On ne pouvait pas encore afficher ici parce que il fallait d'abord parcourir toute la liste
#ici on a trouvé les salaires min et max.
print("------Voici les employés qui ont le salaire le plus élevé:",salaireMax," -------")

for emp in liste:
    if emp["Salaire"] == salaireMax:
        print("Prénom:",emp["Prenom"])

print("------Voici les employés qui ont le salaire le plus bas:",salaireMin," -------")

for emp in liste:
    if emp["Salaire"] == salaireMin:
        print("Prénom:",emp["Prenom"])   


print("-------------------------------")
ecart=salaireMax-salaireMin
print("Écart salarial entre le plus haut et le plus bas:",ecart)
print("-------------------------------")
salaireMoyen=sommeSalaires/nombreEmployes
print("Le salaire moyen est de:",salaireMoyen)

